import juego
import time
import copy


class Bomberman:

    def __init__(self):
        self.pos = [70, 70]
        self.speed = 5
        self.number = 0
        self.alive = True
        self.hitbox = 50
        self.step = None

    def move(self, step, cols):
        """Prompts bomber to move"""
        new_pos = []
        for i, aux in enumerate(self.pos):
            new_pos.append(aux + step[i] * self.speed * cols)
        if cols == 1:
            self.step = step
        return new_pos

    def get_pos(self):
        return self.pos

    def take_damage(self):
        """Npc colision event, kills player"""
        self.alive = False

    def set_pos(self, new_pos):
        """Confirms new pos"""

        self.pos = new_pos

    def get_hitbox(self):
        return self.hitbox

    def drop_bomb(self, cell):
        """Drops a bomb"""
        bomb_drop = cell
        return bomb_drop

    def smoke(self):
        """Explosion colision event, kills player"""
        self.speed = 0
        self.alive = False

    def get_state(self):
        return self.alive
