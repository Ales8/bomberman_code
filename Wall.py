import juego
import time
import copy


class Wall:
    """another bricks the dust"""
    def __init__(self):
        self.pos = None
        self.cell = None
        self.state = "set"
        self.hitbox = 55
        self.border = None

    def get_pos(self):
        return self.pos

    def get_cell(self):
        return self.cell

    def set_pos(self, new_pos, cell):
        self.pos = new_pos
        self.cell = cell

    def get_hitbox(self):
        return self.hitbox

    def smoke(self):
        pass
