import juego
import bomberman
import Bomb
import Wall
import bwall
import control
import pygame
import mapa
import os
import copy


s = {'bomberman': 'killaqueen', 'NPC': 'doggo'}


class Graphics:

    def __init__(self, player, level, game, size):
        pygame.init()
        self.size = size
        self.background = None
        self.model = game
        self.screen = pygame.display.set_mode(self.size)
        self.bomber = None  # Sprite bomberman
        self.player = player  # Bomberman del modelo
        self.link = os.getcwd()  # Ruta de las imágenes
        self.linkb = os.getcwd() + "/bomber"
        self.linkn = os.getcwd() + "/npc"
        self.the_wall, self.obst, self.npc = level  # Paredes del modelo
        self.the_bricks = None  # Sprites de las paredes
        self.the_break = None
        #self.npc = npc # Npc del modleo
        self.enemy = None # Sprite npc 
        self.bomb = None

        self.map = mapa.Main_map(self.player, self.model, self.size, self.npc)

        self.ashes = pygame.image.load('{}/smoke.png'.format(self.link))

        self.load_screen()
        self.load_walls()
        self.load_bwalls()
        self.load_player("", "")
        self.load_npc()
        pygame.key.set_repeat(20)

    def load_screen(self):
        """Loads game screen"""
        self.background = pygame.image.load('{}/dims.png'.format(self.link))
        self.screen.blit(self.background, [0, 0])
        pygame.display.update()

    def refresh(self):
        """Updates game screen"""
        # Gets player location
        white = (255, 255, 255)
        green = (0, 255, 0)

        self.screen.blit(self.background, [0, 0])
        self.draw_bombs()

        for i, w in enumerate(self.the_wall):
            wall, rect_w = self.map.get_rects(w, self.the_bricks)
            self.the_bricks = wall
            self.screen.blit(self.the_bricks, rect_w)

        for i, w in enumerate(self.obst):
            wall, rect_w = self.map.get_rects(w, self.the_break)
            self.the_break = wall
            self.screen.blit(self.the_break, rect_w)
        
        b, p_rect = self.map.get_rects(self.player, self.bomber)
        self.bomber = b
        self.screen.blit(self.bomber, p_rect)
        
        self.load_npc()

        pygame.display.update()

    def load_player(self, dir, step):
        """Loads player character sprite"""
        frame = dir + str(step)
        self.bomber = pygame.image.load('{}/killaqueen{}.png'.format(self.linkb, frame))
        pygame.display.update()

    def load_walls(self):
        """Loads walls on screen"""
        self.the_bricks = pygame.image.load('{}/Standard_wall.png'.format(self.link))
        # Gets wall size
        for i, w in enumerate(self.the_wall):
            wall, rect_w = self.map.get_rects(w, self.the_bricks)
            self.the_bricks = wall
            self.screen.blit(self.the_bricks, rect_w)
        pygame.display.update()

    def load_bwalls(self):
        """Loads obstacles on screen"""
        self.the_break = pygame.image.load('{}/breaker.png'.format(self.link))
        for i, w in enumerate(self.obst):
            wall, rect_w = self.map.get_rects(w, self.the_break)
            self.the_break = wall
            self.screen.blit(self.the_break, rect_w)
        pygame.display.update()

    def draw_bombs(self):
        """Loads all bombs on screen"""
        # Checks if there are bombs
        states, pos, bombs = self.map.check4bombs()
        for i, b in enumerate(states):
            h = self.map.get_hitbox(bombs[i])
            # Checks bombs state
            aux = ((b != "settled") + (b == "fire"))
            self.bomb = pygame.image.load('{}/bomb{}.png'.format(self.link, aux))
            if aux != 2:
                self.bomb, rect_b = self.map.get_rects(bombs[i], self.bomb)
                self.screen.blit(self.bomb, rect_b)
            else:
                self.load_explosion(bombs[i], pos[i])

    def load_explosion(self, bomb, pos):
        """Loads any incoming explosions"""
        exp, smoke = self.map.check4explosion(bomb, pos)

        h = self.map.get_hitbox(bomb)
        angles = [0, 90]
        aux = 0
        mag = (255, 0, 255)

        for i, e in enumerate(exp):
            fire = pygame.transform.scale(self.bomb, (h, (h + 20)))
            fire = pygame.transform.rotate(fire, angles[aux < 2])
            rect_b = pygame.Surface.get_rect(fire, center=e)
            self.screen.blit(fire, rect_b)
            aux = (aux + 1) * (aux < 3)
        for j, s in enumerate(smoke):
            ash = pygame.transform.scale(self.ashes, (h * 2, h * 2))
            rect_a = pygame.Surface.get_rect(ash, center=s)
            self.screen.blit(self.ashes, rect_a)

    def load_npc(self):
        """Loads npc sprite"""
        self.enemy = pygame.image.load('{}/doggo{}.png'.format(self.linkn, "left"))

        for j, e in enumerate(self.npc):
            dir = self.map.get_dir(e)
            sprite = pygame.image.load('{}/doggo{}.png'.format(self.linkn, dir))
            npc, n_rect = self.map.get_rects(e, sprite)
            self.screen.blit(npc, n_rect)
        pygame.display.update()

    def draw_npc(self, npc):
        dir = ""
        self.enemy = pygame.image.load('{}/doggo{}.png'.format(self.linkn, dir))

    def load_gameover(self):
        black = (0, 0, 0)
        self.screen.fill(black)
        dead = pygame.image.load('{}/killaqueendie.png'.format(self.linkb))
        dead, rect_d = self.map.get_rects(self.player, dead)
        self.screen.blit(dead, rect_d)
