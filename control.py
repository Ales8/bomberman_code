import vista
import mapa
import bomberman
import Bomb
import npc
import Wall
import juego
import pygame
import copy
import time
import threading
import os
import menus

pygame.init()
select = [[16, 17, 19, 20, 23, 24, 26, 27, 31, 35, 38, 42, 48, 55, 63, 70, 76, 80, 83, 87, 91, 92, 94, 95, 98, 99, 101, 102,
     121, 122, 124, 125, 128, 129, 131, 132, 136, 140, 143, 147, 153, 160, 166, 170, 173, 177, 181, 182, 184, 185, 188, 189, 191, 192, 
     211, 212, 214, 215, 218, 219, 221, 222, 226, 230, 233, 237, 243, 250, 258, 265, 271, 275, 278, 282, 286, 287, 289, 290, 293, 294, 296, 297 ],
     [16, 17, 18, 19, 20, 23, 24, 25, 26, 27,31,42, 48, 49, 50, 51, 52, 53, 54, 55, 46, 57, 61, 72, 76, 79, 81, 82, 84, 87, 
     91, 94, 99, 109, 114, 124, 126, 127, 129, 136, 147, 151, 153, 154, 155, 156, 157, 158, 159, 160, 162, 166, 177, 184, 186,187, 189, 199, 204, 211, 214, 219, 
     226, 229, 231, 232, 234, 237, 241, 252,256, 258, 259, 260, 261, 262, 263, 264, 265, 267, 271, 282, 286, 287, 288, 289, 290, 293, 294, 295, 296, 297], 
     [16, 17, 19, 22, 24, 27, 31, 34, 36, 37, 39, 42, 46, 48, 49, 52, 54, 55, 57, 77, 80, 82, 83, 86, 91, 92, 93, 95, 98, 100, 101, 102, 107, 110, 111, 113, 116, 
     137, 140, 141, 143, 146, 151, 152, 153, 155, 158 , 160, 161, 162, 167, 170, 171, 173, 176, 
     197, 200, 201, 203,206, 211, 212, 213,215, 218, 220, 221, 222, 227, 230, 232, 233, 236,
      256, 258, 259, 262, 264, 265, 267, 271, 274, 276, 277, 279, 282, 286, 287, 289, 292, 294, 296, 297]]
bwall = [[18,21,22],[2],[2]]


class control:
    def __init__(self):
        self.size = (1100, 750)
        self.is_playing = False
        self.npc = npc.npc()
        self.the_wall = 0
        self.player = None
        self.the_bwall = 0
        self.lvl = None
        self.level = None
        self.model = None
        self.screen = None
        self.menu = None
        self.menu_screen = menus.Menu()

        self.load_menu()

    def load_menu(self):
        """Load start menu loop"""
        running = True
        self.menu_screen.menu_screen()
        while running:
            for event in pygame.event.get():
                keys = pygame.key.get_pressed()
                if event.type == pygame.QUIT:
                    running = False
                if keys[pygame.K_1]:
                    self.the_wall = 82
                    self.level = select[0]
                    self.the_bwall = 3
                    self.lvl = bwall[0]
                    self.is_playing = True
                    self.play()
                    running = False
                if keys[pygame.K_2]:
                    self.the_wall = 92
                    self.level = select[1]
                    self.the_bwall = 1
                    self.lvl = bwall[1]
                    self.is_playing = True
                    self.play()
                    running = False
                if keys[pygame.K_3]:
                    self.the_wall = 93
                    self.level = select[2]
                    self.the_bwall = 0
                    self.lvl = bwall[2]
                    self.is_playing = True
                    self.play()
                    running = False
            pygame.display.update()

    def play(self):
        """Main game loop"""
        self.player = bomberman.Bomberman()
        self.model = juego.Game(self.player, self.the_wall, self.level, self.size, self.npc, self.the_bwall, self.lvl)
        self.screen = vista.Graphics(self.player, self.model.build_level(),
                                     self.model, self.size)
        p_state = True
        BOMBEVENT = pygame.USEREVENT + 1
        CHECKEVENT = pygame.USEREVENT + 2
        ENEMYEVENT = pygame.USEREVENT + 3
        pygame.time.set_timer(BOMBEVENT, 1000)
        pygame.time.set_timer(CHECKEVENT, 100)
        pygame.time.set_timer(ENEMYEVENT, 1000)
        c = 0
        step = 0
        while self.is_playing:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.is_playing = False
                    p_state = False
                pygame.display.flip()
                keys = pygame.key.get_pressed()
                if keys[pygame.K_LEFT]:
                    dir = "left"
                    self.model.move_asset(dir, self.player)
                    self.screen.load_player(dir, step)
                    self.screen.refresh()
                    step = step + 1
                    step = step * (step <= 7)
                
                if keys[pygame.K_RIGHT]:
                    dir = "right"
                    self.model.move_asset(dir, self.player)
                    self.screen.load_player(dir, step)
                    self.screen.refresh()
                    step = step + 1
                    step = step * (step <= 7)
                if keys[pygame.K_UP]:
                    dir = "up"
                    self.model.move_asset(dir, self.player)
                    self.screen.load_player(dir, step)
                    self.screen.refresh()
                    step = step + 1
                    step = step * (step <= 7)
               
                if keys[pygame.K_DOWN]:
                    dir = "down"
                    self.model.move_asset(dir, self.player)
                    self.screen.load_player(dir, step)
                    self.screen.refresh()
                    step = step + 1
                    step = step * (step <= 7)
                
                if keys[pygame.K_SPACE]:
                    # Bomb is placed with given timestamp
                    self.model.bomb_drop(self.player, c)
                    self.screen.refresh()

                if keys[pygame.K_ESCAPE]:
                    self.load_menu()

                if event.type == BOMBEVENT:
                    self.model.detonate(c)
                    self.screen.draw_bombs()
                    c = c + 1

                if event.type == CHECKEVENT:
                    self.model.npc_movement()
                    p_state = self.player.get_state()
                    self.screen.refresh()
                    if not p_state:
                        self.screen.refresh()
                        self.screen.draw_bombs()
                        break

                if event.type == ENEMYEVENT:
                    self.model.update_pattern()
                    self.screen.refresh()
                
                step = step * (step <= 7)

            self.is_playing = p_state
        time.sleep(2)
        self.game_over()
 
    def game_over(self):
        """Loads game-over screen loop"""
        self.screen.load_gameover()
        self.menu_screen.game_over()
        running = True
        while running:
            for event in pygame.event.get():
                keys = pygame.key.get_pressed()
                if event.type == pygame.QUIT:
                    running = False
                    quit()
                if keys[pygame.K_1]:
                    self.load_menu()
                    running = False
                if keys[pygame.K_2]:
                    running = False
            pygame.display.update()


if __name__ == "__main__":
    controlador = control()
