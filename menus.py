import pygame
import control
import vista
import mapa
import os


class Menu:
    def __init__(self):
        self.menu = pygame.display.set_mode([1100, 750])

    def menu_screen(self):
        """Main menu screen view"""
        background = pygame.image.load('{}/main.png'.format(os.getcwd()))
        background = pygame.transform.scale(background, (1100, 750))
        r = pygame.Surface.get_rect(background, topleft=(0, 0))
        self.menu.blit(background, r)

    def game_over(self):
        background = pygame.image.load('{}/gameover.jpg'.format(os.getcwd()))
        r = pygame.Surface.get_rect(background, center=(550, 550))
        self.menu.blit(background, r)
