import pygame
import juego
import control


class Main_map:
    def __init__(self, player, game, size, npc):
        self.model = game
        self.size = size
        self.npc = npc
        self.player = player
        self.matrix = self.model.get_cells()

    def get_coordinates(self, asset):
        """Returns current position of a given asset"""
        asset_pos = asset.get_pos()
        return asset_pos

    def get_hitbox(self, asset):
        """Returns hotbox value"""
        return asset.get_hitbox()

    def get_rects(self, asset, sprite):
        """Returns rescaled and centred sprite"""
        h = self.get_hitbox(asset)
        p = asset.get_pos()
        sprite = pygame.transform.scale(sprite, (h, h))
        a_rect = pygame.Surface.get_rect(sprite, center=p)
        return sprite, a_rect

    def check4bombs(self):
        """Returns lists with all the instances, states
        and positions of all bombs in the map"""
        bombs = self.model.bomb_check()
        state = []
        pos = []
        exp = []
        for i, b in enumerate(bombs):
            state.append(b.get_state())
            pos.append(b.get_pos())
        return state, pos, bombs

    def check4explosion(self, bomb, pos):
        """Checks for any explosions in the map"""
        exp, smoke = bomb.get_fire()
        exp_pos = []
        smoke_pos = []
        for i, e in enumerate(exp):
            exp_pos.append(self.matrix[e])
        for i, s in enumerate(smoke):
            exp_pos.append(self.matrix[s])
        return exp_pos, smoke_pos
    
    def get_state(self, asset):
        return asset.get_state()

    def get_dir(self, npc):
        return npc.get_path()
    