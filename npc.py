import juego
import time
import copy
import random

directions = ['left',  'right', 'up', 'down']


class npc():

    def __init__(self):
        self.pos = [300, 200]
        self.speed = 5
        self.state = "alive"
        self.hitbox = 45
        self.path = directions[0]

    def move(self, step, cols):
        """Prompts npc to move"""
        new_pos = []
        for i, aux in enumerate(self.pos):
            new_pos.append(aux + step[i] * self.speed * cols)
        if cols == 0:
            self.path = directions[random.randrange(4)]
        return new_pos

    def get_path(self):
        return self.path

    def set_path(self):
        self.path = directions[random.randrange(4)]

    def get_pos(self):
        return self.pos

    def set_pos(self, new_pos):
        self.pos = new_pos

    def get_hitbox(self):
        return self.hitbox

    def get_state(self):
        return self.state

    def smoke(self):
        """Explosion colision event, 'kills' the npc"""
        self.state = "fire"
        self.speed = 0

    def take_damage(self):
        """npc colision event"""
        pass
