import control
import bomberman
import Bomb
import Wall
import bwall
import npc
import copy
import time

direct = {'left': [-1, 0], 'right': [1, 0], 'up': [0, -1], 'down': [0, 1]}


class Game:

    def __init__(self, player, the_wall, level, size, npc, the_bwall, lvl):
        self.lvl = lvl  # Breakable wall (b-wall) cells list
        self.size = size  # Game screen size
        self.walls = the_wall  # Initially, holds amount of walls
        self.bwalls = the_bwall  # Initially, holds amount of b-walls
        self.level = level  # Non breakeble wall object list
        self.bombs = []  # Active bombs cells list
        self.bomb = Bomb.Bomb()
        self.obstacles = []  # B-wall object list
        self.matrix = []  # All game matrix cells position list
        self.board = []  # Parallel game matrix, holds all in-game objects
        self.b = 50  # Game cell size
        self.player = player
        self.npc = npc
        self.enemy = []  # NPC object list
        # Falta lista de niveles.

        self.set_cells()
        self.check_cell(player.get_pos())

    def set_cells(self):
        """Builds game matrix. The game matrix is a list of positions
           which are the center of the game board's cells."""
        c1 = self.size[0] // self.b  # Splits screen width in cells
        c2 = self.size[1] // self.b  # Splits screen height in cells
        aux = 0
        for i in range(c1):  # Cells list is built verically(x, y1, etc)
            auy = 0
            aux = aux + self.b
            for i in range(c2):
                auy = auy + self.b
                self.matrix.append([aux, auy])
        for j in range(len(self.matrix)):
            self.board.append(None)  # Builds object matrix

    def get_cells(self):
        """Returns game matrix."""
        return self.matrix

    def build_level(self):
        """Builds selected level with the corresponding values
        (wall positions and amount, npc's and intial positions."""
        # Creates base wall objects
        a_wall = Wall.Wall()
        a_bwall = bwall.B_Wall()
        # Creates wall lists
        the_wall = []
        the_bwall = []
        for i in range(self.walls):  # Walls are set in cells
            w = copy.deepcopy(a_wall)
            w.set_pos(self.matrix[self.level[i]], self.level[i])
            the_wall.append(w)
            self.board[self.level[i]] = w

        for i in range(self.bwalls):  # B-walls are set in cells
            x = copy.deepcopy(a_bwall)
            x.set_pos(self.matrix[self.lvl[i]], self.lvl[i])
            the_bwall.append(x)
            self.board[self.lvl[i]] = x

        self.walls = the_wall
        self.obstacles = the_bwall

        self.enemy.append(copy.deepcopy(self.npc))
        return the_wall, the_bwall, self.enemy

    def move_asset(self, dir, asset):
        """Prompts assets to move"""
        path = direct[dir]  # Movement value determined through direction
 
        # 'Old' position is erased from game matrix
        old_cell, p = self.check_cell(asset.get_pos())
        self.board[old_cell] = None

        # Takes a 'would be' new position with current path and no colsions
        new_cell, new_pos = self.check_cell(asset.move(path, 1))
        # Returns colsions values
        colisions_w, colisions_npc = self.colisions(new_cell, new_pos)
        # New position is confirmed
        asset.set_pos(asset.move(path, colisions_w))

        # Game matrix is updated with new position cell
        if colisions_w == 1:
            self.board[new_cell] = asset
        else:
            self.board[old_cell] = asset
        if colisions_npc:
            asset.take_damage()

    def check_cell(self, pos):
        """Checks asset's current cell in game matrix"""
        b1 = self.b // 2
        # Round up pos values to cell size value (b)
        x_aux = [(pos[0] - pos[0] % self.b), (pos[0] + (self.b - pos[0] % self.b))]
        y_aux = [(pos[1] - pos[1] % self.b), (pos[1] + (self.b - pos[1] % self.b))]
        x = x_aux[(pos[0] - pos[0] % b1) > (pos[0] - pos[0] % self.b)]
        y = y_aux[(pos[1] - pos[1] % b1) > (pos[1] - pos[1] % self.b)]

        # Cell count from x value
        cx = (x // self.b - 1) * (self.size[1] // self.b)

        if x > self.size[0]:  # Check overflow in round up
            x = self.size[0]
            cx = (x // self.b - 1) * (self.size[1] // self.b)
        if y > self.size[1]:
            y = self.size[1]

        cy = cx + self.size[1] // self.b

        # Where cx is the starting width cell value
        # and cy is the amount of y values in x cell value
        for i in range(cx, cy):
            if self.matrix[i][1] >= y:  # Locates current cell
                follow = (self.matrix[i][1] < pos[1])
                aux = [self.matrix[i][1] - pos[1], pos[1] - self.matrix[i][1]]
                if aux[follow] <= self.b:
                    cell = i
                    break
        return cell, pos

    def colisions(self, cell, new_pos):
        """Receives a map cell and a position, returns colisions with all walls
           and npc's in contact"""
        b = self.b // 2

        # Checks if position is within screen limits
        pos_aux1 = ((0 < (new_pos[0] - b)) * ((new_pos[0] + b) < self.size[0]) *
                    (0 < (new_pos[1] - b)) * ((new_pos[1] + self.b) <= self.size[1]))

        # Checks if cell is already taken
        pos_aux2 = self.board[cell] is None

        # Checks if cell is not overflow
        pos_aux3 = (cell >= 0) * (cell < len(self.matrix))

        colisions = pos_aux1 * pos_aux2 * pos_aux3  # Normal colsions

        clash = self.board[cell] in self.enemy  # Colsions with npc

        return colisions, clash

    def bomb_drop(self, player, timestamp):
        """Player drops bomb"""
        bomb = copy.deepcopy(self.bomb)  # Creates bomb
        # Sets bomb location and timestamp for countdown
        cell, pos = self.check_cell(player.get_pos())
        drop = self.matrix[cell]
        bomb.set_loc(player.drop_bomb(drop), timestamp, cell)
        self.board[cell] = bomb
        # Adds bomb to active bomb list
        self.bombs.append(bomb)
        # Starts countdown sequence
        self.detonate(timestamp)

    def bomb_check(self):
        """Retruns list with all in-game bombs"""
        return self.bombs

    def detonate(self, timed):
        """Checks for bomb state"""
        for i, b in enumerate(self.bombs):
            self.board[b.get_cell()] = b
            b.set_state(timed)
            if b.get_state() != "settled":
                self.explosion(b)

    def explosion(self, bomb):
        """blows up bomb"""
        aux = [1] * 4
        exp = bomb.destroy(aux)
        cols = []
        down = []
        for i, e in enumerate(exp):
            col, np = self.colisions(e, self.matrix[e])
            cols.append(col)
            if col == 0 and self.board[e] is not None:
                down.append(e)
                print("EE", self.board[e])
                self.board[e].smoke()
        final = bomb.destroy(cols)
        bomb.set_pos(final, down)
        state = bomb.get_state()
        if (state == "dust"):
            for j, f in enumerate(down):
                self.remove_corpses(down)
                if self.board[f] == self.player:
                    self.player.smoke()
            self.bombs.remove(bomb)
            self.board[bomb.get_cell()] = None
            del bomb

    def npc_movement(self):
        """Propmts npc to start movement sequence"""
        for e, npc in enumerate(self.enemy):
            step = npc.get_path()
            self.move_asset(step, npc)

    def update_pattern(self):
        """Prompts npc's to update their movement direction"""
        self.npc.set_path()

    def remove_corpses(self, corpses):
        """Removes all 'dead' non-player objects on the board"""
        for i, w in enumerate(corpses):
            if self.board[w] in self.obstacles:
                self.obstacles.remove(self.board[w])
                self.board[w] = None
                del w
                continue
            if self.board[w] in self.enemy:
                self.enemy.remove(self.board[w])
                self.board[w] = None
                del w
