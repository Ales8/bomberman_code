import juego
import time
import copy
import Wall


class B_Wall(Wall.Wall):
    """Down comes the wall"""
    def __init__(self):
        self.state = "set"
        self.pos = [70, 140]
        self.cell = None
        self.hitbox = 55
        self.border = None

    def get_state(self):
        return self.state

    def smoke(self):
        """Explosion colision event, 'destroys' the b-wall"""
        self.state = "smoke"
