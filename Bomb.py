import juego
import time
import copy


class Bomb:
    """Set us up the bomb"""
    def __init__(self):
        self.pos = None
        self.range = 3
        self.esplosion = None
        self.timer = 0
        self.hitbox = 50
        self.speed = 3
        self.timestamp = None
        self.hitbox = 40
        self.cell = None
        # 'settled' / 'explosion'
        self.state = None
        self.fire = []

    def get_hitbox(self):
        return self.hitbox

    def set_loc(self, loc, timed, new_cell):
        self.cell = new_cell
        self.pos = loc
        self.state = "settled"
        self.timestamp = timed
        self.esplosion = [(self.cell)] * 4
        self.fire = self.fire + self.esplosion
        self.ash = []

    def get_pos(self):
        return self.pos

    def get_state(self):
        return self.state

    def get_cell(self):
        return self.cell

    def set_state(self):
        pass

    def get_exp(self):
        return self.esplosion

    def set_pos(self, new_exp, smoke):
        self.esplosion = new_exp
        self.range = self.range - 1
        self.fire = self.fire + self.esplosion
        self.ash = self.ash + smoke

    def get_fire(self):
        return self.fire, self.ash

    def set_state(self, timed):
        states = ["settled", "explosion", "fire", "dust"]
        det = ((timed - self.timestamp >= 5) +
               (timed - self.timestamp > 6) +
               (self.range <= 0))
        self.state = states[det]
        if self.state == "dust":
            self.erase
        return self.state

    def destroy(self, cols):
        path = [-15, 15, -1, 1]
        exp = []
        explosion = self.esplosion
        for i, e in enumerate(explosion):
            aux = e + path[i] * cols[i]
            exp.append(aux)
        return exp

    def erase(self):
        self.state = "dust"

    def smoke(self):
        self.erase()